# http://rubular.com
# http://www.tutorialspoint.com/ruby/ruby_regular_expressions.htm
# http://lzone.de/Ruby-Regex-Examples
require 'date'

class Regex
	attr_accessor :string , :match_data

	def initialize(gerd="not defined")
		@string = "not defined"
		@string = gerd unless gerd== nil
	end

	def is_url?
		@match_data = /\.com|\.org|\.gov|\.edu/.match(@string) 
		unless @match_data != nil
			return false
		end
		return true
	end

	def is_name?
		return false
	end

	def is_date?
		# ([00-99]{1}|[1-2014]{1})   year
		# @match_data = /[0-1][0-9]/[0-3][0-9]/[0-9]{2}(?:[0-9]{2})?/.match(@string) 
		begin
			 Date.parse(@string)
		rescue 
			return false
		end
		return true
	end

	def is_phone_number?
		return_value=false
		# @match_data = /(?:\+?|\b)[0-9]{10}\b/.match(@string) 
		@match_data = /(?:\+?|\b)1?[0-9]{3}-?[0-9]{3}-?[0-9]{4}\b/.match(@string) 
		if @match_data != nil
			return_value=true
		end
		return return_value
	end

	def is_president_mentioned?
		@match_data = /(ob(a|o)ma)|(president)/i.match(@string) 
		if @match_data != nil
			@match_data = /clinton|bush|reagan|carter/i.match(@string) 
			if @match_data == nil
				return true
			else
				return false
			end
		end
		return false
	end

	def get_value
		return @string
	end
end
