require 'minitest/autorun'
require './regex'

class RegExTest < MiniTest::Unit::TestCase

  def setup
  end
  
  
  def teardown
  end

  def test_basic
    has = {
      "one" => "1",
      "two" => "2",
      "three" => "3"
    }

    assert_equal true , true 
    # expected vs actual 
  end

  def test_attr_accessor_works
    n = Regex.new
    n.string="gerd"
    assert_equal "gerd", n.get_value
  end

  def test_detects_valid_url
    n = Regex.new
    text_to_parse = "www.google.com"
    n.string = text_to_parse
    assert_equal true, n.is_url?

    text_to_parse = "http://bing.com"
    n.string = text_to_parse
    assert_equal true, n.is_url?

    n = Regex.new
    text_to_parse = "www.bitbucket.org"
    n.string = text_to_parse
    assert_equal true, n.is_url?

    text_to_parse = "https://goarmy.gov"
    n.string = text_to_parse
    assert_equal true, n.is_url?

    text_to_parse = "http://illinois.edu"
    n.string = text_to_parse
    assert_equal true, n.is_url?
  end

  def test_detects_invalid_url
    n = Regex.new

    text_to_parse = "gerd"
    n.string = text_to_parse
    assert_equal false, n.is_url?

    text_to_parse = "edu"
    n.string = text_to_parse
    assert_equal false, n.is_url?

     text_to_parse = "http://gerd"
    n.string = text_to_parse
    assert_equal false, n.is_url?
  end

  
  
  def test_detects_valid_date
    n = Regex.new

    text_to_parse = "10-5-12"
    n.string = text_to_parse
    assert_equal true, n.is_date?
    text_to_parse = "31/03/2002"
    n.string = text_to_parse
    assert_equal true, n.is_date?

    text_to_parse = "dec-1-2013"
    n.string = text_to_parse
    assert_equal true, n.is_date?

    text_to_parse = "february-15-1956"
    n.string = text_to_parse
    assert_equal true, n.is_date?
  end

  def test_detects_invalid_date
    n = Regex.new

    text_to_parse = "02-31-2012"
    n.string = text_to_parse
    assert_equal false, n.is_date?

    text_to_parse = "01-45-2012"
    n.string = text_to_parse
    assert_equal false, n.is_date?

    text_to_parse = "hekedldld-1a956"
    n.string = text_to_parse
    assert_equal false, n.is_date?

    text_to_parse = "tuesday-1954-05"
    n.string = text_to_parse
    assert_equal false, n.is_date?
  end

  def test_detects_invalid_url
    n = Regex.new

    text_to_parse = "gerd"
    n.string = text_to_parse
    assert_equal false, n.is_date?

     text_to_parse = "http://gerd"
    n.string = text_to_parse
    assert_equal false, n.is_date?
  end

  def test_demo
    assert(true, "what?")
  end


 

end
