# \A beginning string    \z end of string    \Z end of string minus newline  ^ begging of a new line $ end of line 
# ignore unescaped whitespace /x   

s = "good morning i am a string"
m = /mor/.match(s)
puts m 
puts m.inspect
puts m.pre_match
puts m.post_match

str = " Foo .gerdy"
puts str=~ /ger/ #returns offset of the match
puts str=~ /rob/ 

# regex SCAN method
m = "I am skrillex the american dj amigo" 
ar = m.scan(/am/)
p ar 
ar = m.scan(/(.)am/)
p ar.inspect  

# sub and gsub
string = 'the rain in spain'
puts string.sub(/in/,"**")
puts string.gsub(/in/,"**")

puts string.sub!(/in/,"IN")# bang permenatly changes it 
string.gsub!.(/IN/) {|match| match.reverse}

puts string


# m = /mor/.match(s)
# puts m 
# m = /mor/.match(s)
# puts m 
# m = /mor/.match(s)
# puts m 
# m = /mor/.match(s)
# puts m 
# m = /mor/.match(s)
# puts m 
# m = /mor/.match(s)
#  