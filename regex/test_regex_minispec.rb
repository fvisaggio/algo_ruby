require 'minitest/autorun'
require_relative 'regex'

describe Regex do 

	describe "when initialized" do 
		it "must set the regex string" do
			input = "panama"
			obj = Regex.new(input) 
			obj.string.must_equal(input)
		end

		it "must defualt to proper value if not set" do
			obj = Regex.new 
			obj.string.must_equal("not defined")
		end 
	end

	describe "when looking for phone numbers" do 

		it "must parse valid phone numbers as true" do 
			regex = Regex.new
			phone_number = "8002019878"
    		regex.string = phone_number
    		regex.is_phone_number?.must_equal(true)
		end
		it "must parse phone numbers that start with +" do 
			regex = Regex.new
			phone_number = "+8002019878"
    		regex.string = phone_number
    		regex.is_phone_number?.must_equal(true)
		end
		it "must parse valid phone numbers with dashes" do 
			regex = Regex.new
			phone_number = "800-201-9878"
    		regex.string = phone_number
    		regex.is_phone_number?.must_equal(true)
		end
		it "must parse phone numbers that start with 1800 " do 
			regex = Regex.new
			phone_number = "1800-201-9878"
    		regex.string = phone_number
    		regex.is_phone_number?.must_equal(true)
    		phone_number = "1-800-201-9878"
    		regex.string = phone_number
    		regex.is_phone_number?.must_equal(true)
		end
		it "must reject zip codes " do 
			regex = Regex.new
			phone_number = "07470"
    		regex.string = phone_number
    		regex.is_phone_number?.must_equal(false)
    	end
    	it "must reject phone numbers with letters " do 
			regex = Regex.new
			phone_number = "1-800-CALL-ATT"
    		regex.string = phone_number
    		regex.is_phone_number?.must_equal(false)
    	end

	end

	describe "when looking for The President" do 

	regex = Regex.new
		it "must detect presence of Obama" do 
			regex.string = "Do we have Mr. Obama on the phone"
    		regex.is_president_mentioned?.must_equal(true)
		end
		it "must detect typos of Obama " do 
			regex.string = "Whats the American presidents name is it Oboma"
    		regex.is_president_mentioned?.must_equal(true)
		end
		it "must detect lowercase " do 
			regex.string = "obama is visiting us today"
    		regex.is_president_mentioned?.must_equal(true)
		end
		it "must parse phone numbers that start with 1800 " do 
			regex.string = "Is the president always late"
    		regex.is_president_mentioned?.must_equal(true)
		end
		it "must must not match on clinton " do 
    		regex.string = "Wasnt Clinton a president a while back"
    		regex.is_president_mentioned?.must_equal(false)
    	end
    	it "must not match on other previous presidents " do 
    		regex.string = "Reagan and Bush were outstanding rock climbers"
    		regex.is_president_mentioned?.must_equal(false)
    	end

	end

	describe "when all is well" do
		it "will always pass because this is an example" do 
			true.must_equal(true)
		end

	end

end